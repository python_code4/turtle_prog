import turtle
screen = turtle.Screen()
screen.setup(320,320) 
screen.tracer(0) 
screen.title('TURTLE ANIMATION USING ARROW KEYS')
trtl = turtle.Turtle()
trtl.hideturtle()

x, y, x_step, y_step = 0, 0, 10, 10 

def next_frame():
    trtl.clear()
    trtl.penup()
    trtl.goto(x, y)
    trtl.pendown()
    trtl.dot(20) 
    screen.ontimer(next_frame, 10)
    screen.update()

def move_left():
    global x
    print('left arrow key pressed')
    x = x - x_step
 
def move_right():
    global x
    print('right arrow key pressed')
    x = x + x_step
  
def move_down():
    global  y
    print('down arrow key pressed')
    y = y - y_step
 
def move_up():
    global y
    print('up arrow key pressed')
    y = y + y_step
  
screen.onkey(move_left, 'Left')
screen.onkey(move_right, 'Right')
screen.onkey(move_down, 'Down')
screen.onkey(move_up, 'Up')
 
next_frame()
screen.listen()
screen.mainloop()